package com.company;

import com.company.lang.BinaryNumber;
import com.company.lang.RSA;

import java.io.*;

public class RSAMain {
	public static void main(String[] args) throws IOException {
		try (
				BufferedReader in = new BufferedReader(new FileReader("in_rsa.txt"));
				PrintWriter out_decrypt = new PrintWriter(new BufferedWriter(new FileWriter("out_rsa_decrypt.txt")));
				PrintWriter out_encrypt = new PrintWriter(new BufferedWriter(new FileWriter("out_rsa_encrypt.txt")))

		) {
			RSA rsa = new RSA();
			BinaryNumber number = rsa.encryptRSA(new BinaryNumber(in.readLine(), true));
			out_encrypt.println(number);
			out_encrypt.println(rsa.publicKey);
			out_encrypt.println(rsa.privateKey);
			out_decrypt.println(rsa.decryptRSA(number));
		}
	}
}
