package com.company.waste;

import com.company.lang.BinaryNumber;
import com.company.lang.Key;
import com.company.lang.Util;

import java.util.Arrays;

public class RSA8 {
	public Key publicKey = new Key(new BinaryNumber(Integer.toBinaryString(3)), new BinaryNumber(Integer.toBinaryString(9173503))), privateKey = new Key(new BinaryNumber(Integer.toBinaryString(6111579)), new BinaryNumber(Integer.toBinaryString(9173503)));

	public BinaryNumber encryptOAEP(BinaryNumber number) {

		String s = number.toStringBinaryValidate();
		String s1 = s + "01" + new String(new char[8 - s.length() - 2]).replace("\0", "0");

		number = new BinaryNumber(s1);

		BinaryNumber random1 = BinaryNumber.generate(8, false);
		BinaryNumber shaRandom = getHash8bit(random1);
		BinaryNumber x1 = BinaryNumber.xor(number, shaRandom, 1);
		BinaryNumber shaX = getHash8bit(x1);
		BinaryNumber y1 = BinaryNumber.xor(shaX, random1, 1);

		return encryptRSA(new BinaryNumber(Util.concatArrayByte(x1.getValue(), y1.getValue())));
	}


	public BinaryNumber decryptOAEP(BinaryNumber number) {
		BinaryNumber m = decryptRSA(number);
		byte[] value = m.getValue();
		int l = value.length / 2;
		BinaryNumber x = new BinaryNumber(Arrays.copyOfRange(value, 0, l));
		BinaryNumber y = new BinaryNumber(Arrays.copyOfRange(value, l, value.length));
		BinaryNumber shaX_1 = getHash8bit(x);
		BinaryNumber random = BinaryNumber.xor(shaX_1, y, 1);
		BinaryNumber message = BinaryNumber.xor(x, getHash8bit(random), 1);
		String mes = message.toString();
		mes = mes.substring(0, mes.lastIndexOf("1") - 1);
		return new BinaryNumber(mes);
	}

	public BinaryNumber encryptRSA(BinaryNumber number) {
		if (publicKey == null) throw new NullPointerException("Public key is null");
		if (publicKey.second.compareTo(number) <= 0)
			throw new IllegalArgumentException("Length N > p*q");

		BinaryNumber answer = BinaryNumber.fastPowToMod(number, publicKey.first, publicKey.second);
		//answer = new BinaryNumber(Util.concatArrayByte(new byte[128 - answer.getValue().length], answer.getValue()));
		return answer;

	}

	public BinaryNumber decryptRSA(BinaryNumber number) {
		if (privateKey == null) throw new NullPointerException("Private key is null");
		BinaryNumber answer = BinaryNumber.fastPowToMod(
				number,
				privateKey.first, privateKey.second
		).getBinaryValidate();
		/*if (answer.getValue().length < 128)
			answer = new BinaryNumber(
					Util.concatArrayByte(
							new byte[128 - answer.getValue().length],
							answer.getValue())
			);*/
		/*else if (answer.getValue().length > 128)
			answer = new BinaryNumber(
			Arrays.copyOfRange(answer.getValue(), answer.getValue().length - 127, answer.getValue().length));
		*/
		return answer;
	}

	public static BinaryNumber getHash8bit(BinaryNumber number) {
		byte[] bytes = number.getValue();

		byte checksum = 0;
		for (byte aByte : bytes) {
			checksum = (byte) ((checksum + aByte) & 0xFF);
		}
		checksum = (byte) (((checksum ^ 0xFF) + 1) & 0xFF);
		return new BinaryNumber(new byte[]{checksum});

	}

}
