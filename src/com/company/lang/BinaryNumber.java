package com.company.lang;


import java.math.BigInteger;
import java.nio.file.Path;
import java.util.Arrays;

//Реализация класса бинарный чисел
public class BinaryNumber implements Comparable<BinaryNumber> {
	public static final byte ZERO_BYTE = 0;
	public static final BinaryNumber EMPTY = new BinaryNumber(new byte[0]);
	public static final BinaryNumber ZERO = new BinaryNumber(new byte[]{ZERO_BYTE});
	public static final BinaryNumber ONE = new BinaryNumber(new byte[]{1});
	public static final BinaryNumber TWO = addOne(ONE);

	private byte[] value;
	//Колличество нулей в старных битах
	private int firstZero = 0;

	public BinaryNumber(byte[] value) {
		this.value = value;
	}

	public BinaryNumber(int value) {
		this(Integer.toBinaryString(value));
	}

	public BinaryNumber(String binaryNumber) {
		if (!validate(binaryNumber)) throw new IllegalArgumentException();

		if (binaryNumber.isEmpty())
			value = new byte[0];
		else {

			int k = binaryNumber.length() % 8;
			k = (k == 0) ? 8 : k;
			binaryNumber = new String(new char[8 - k]).replace("\0", "0") + binaryNumber;

			int size = binaryNumber.length() / 8;
			value = new byte[size];

			for (int i = 0; i < size; i++) {
				value[i] = Util.stringToByte(binaryNumber.substring(8 * i, 8 * (i + 1)));
			}
		}
	}

	//Конструктор для сохранения нулей в старших битах
	public BinaryNumber(String binaryNumber, boolean withZero) {
		this(binaryNumber);
		if (withZero && binaryNumber.contains("1")) firstZero = binaryNumber.indexOf("1");
	}


	//Возврашает правильное бинарное число
	public BinaryNumber getBinaryValidate() {
		byte[] value = this.value;
		if (value.length != 0)
			for (int i = 0, l = value.length; i < l; i++)
				if (value[i] != 0) {
					if (i != 0) value = Arrays.copyOfRange(value, i, value.length);
					break;
				}

		return new BinaryNumber(value);
	}

	//Рандомная генерация бинарных чисел
	public static BinaryNumber generate(int length, boolean validateBinaryNumber) {
		if (length < 0) throw new IllegalArgumentException();
		if (length == 0) return EMPTY;
		char[] chars = new char[length];
		chars[0] = '1';
		for (int i = (!validateBinaryNumber) ? 0 : 1; i < length; i++)
			chars[i] = (char) ('0' + (int) (Math.random() * 2));

		return new BinaryNumber(String.valueOf(chars));
	}


	//Генерация нечетных рандомных чисел
	public static BinaryNumber generateNoParity(int length, boolean validateBinaryNumber) {
		if (length < 0) throw new IllegalArgumentException();
		if (length == 0) return EMPTY;
		char[] chars = new char[length];
		chars[0] = '1';
		for (int i = (!validateBinaryNumber) ? 0 : 1; i < length; i++)
			chars[i] = (char) ('0' + (int) (Math.random() * 2));

		chars[length - 1] = '1';
		return new BinaryNumber(String.valueOf(chars));
	}

	//Генерация на отрезке длины
	public static BinaryNumber generate(int minLength, int maxLength, boolean validateBinaryNumber) {
		if (maxLength < minLength) throw new IllegalArgumentException();
		return generate(minLength + (int) (Math.random() * (maxLength - minLength)), validateBinaryNumber);
	}

	//Поиск НОД
	public static BinaryNumber findGcd(BinaryNumber first, BinaryNumber second) {
		BinaryNumber k = new BinaryNumber("1");
		while (!first.isZero() && !second.isZero()) {
			while (first.isParity() && second.isParity()) {
				String f = first.toString(), s = second.toString();
				first = new BinaryNumber(f.substring(0, f.length() - 1));
				second = new BinaryNumber(s.substring(0, s.length() - 1));
				k = addition(k, k);
			}

			while (first.isParity()) {
				String f = first.toString();
				first = new BinaryNumber(f.substring(0, f.length() - 1));
			}

			while (second.isParity()) {
				String s = second.toString();
				second = new BinaryNumber(s.substring(0, s.length() - 1));
			}

			if (first.compareTo(second) >= 0) first = sub(first, second);
			else second = sub(second, first);
		}

		return fastMultiplication(k, second);
	}

	//Поиск обратного числа (Алгоритм Евклида)
	public static BinaryNumber findExtendedEuclidean(BinaryNumber a, BinaryNumber b) {

		if (a.compareTo(b) < 0) throw new IllegalArgumentException();

		if (b.isZero())
			return ZERO;
		BinaryNumber m = a;
		boolean[] flags = new boolean[10];
		// a  - 0,
		// b  - 1,
		// x1 - 2,
		// x2 - 3,
		// y1 - 4,
		// y2 - 5,
		// r  - 6,
		// q  - 7,
		// x  - 8,
		// y  - 9  отрицание = true
		BinaryNumber x1 = ZERO, x2 = ONE, y1 = ONE, y2 = ZERO;
		BinaryNumber r, q, x, y;

		Pair<BinaryNumber, Boolean> tmp;
		while (!b.isZero() & !flags[1]) {

			flags[7] = flags[0] ^ flags[1];
			q = division(a, b);

			//r = sub(a, fastMultiplication(q, b));
			tmp = specialSubMult(a, flags[0], q, flags[7], b, flags[1]);
			r = tmp.first;
			flags[6] = tmp.second;

			//x = sub(x2,fastMultiplication(q, x1));
			tmp = specialSubMult(x2, flags[3], q, flags[7], x1, flags[2]);
			x = tmp.first;
			flags[8] = tmp.second;

			//y = sub(y2,fastMultiplication(q,y1));
			tmp = specialSubMult(y2, flags[5], q, flags[7], y1, flags[4]);
			y = tmp.first;
			flags[9] = tmp.second;


			flags[0] = flags[1];
			a = b;

			flags[1] = flags[6];
			b = r;

			flags[3] = flags[2];
			x2 = x1;

			flags[2] = flags[8];
			x1 = x;

			flags[5] = flags[4];
			y2 = y1;

			flags[4] = flags[9];
			y1 = y;
			//   3.1 q:=[a/b], r:=a-qb, x:=x2-qx1, y:=y2-qy1
			//   3.2 a:=b, b:=r, x2:=x1, x1:=x, y2:=y1, y1:=y
		}

		if (flags[5])
			y2 = sub(m, y2);

		return y2;
	}

	private static Pair<BinaryNumber, Boolean> specialSubMult(BinaryNumber first, boolean bF,
	                                                          BinaryNumber second, boolean bS,
	                                                          BinaryNumber third, boolean bT) {
		//r = sub(a, fastMultiplication(q, b));
		BinaryNumber tmp = fastMultiplication(second, third).getBinaryValidate();
		BinaryNumber answer = null;
		boolean answerFlag;
		if (bS ^ bT) {
			if (bF) {
				int i = first.compareTo(tmp);
				if (i > 0) {
					answer = sub(first, tmp);
					answerFlag = true;
				} else if (i == 0) {
					answer = ZERO;
					answerFlag = false;
				} else {
					answer = sub(tmp, first);
					answerFlag = false;
				}

			} else {
				answer = addition(first, tmp);
				answerFlag = false;
			}
		} else {
			if (bF) {
				answer = addition(first, tmp);
				answerFlag = true;
			} else {
				int i = first.compareTo(tmp);
				if (i > 0) {
					answer = sub(first, tmp);
					answerFlag = false;
				} else if (i == 0) {
					answer = ZERO;
					answerFlag = false;
				} else {
					answer = sub(tmp, first);
					answerFlag = true;
				}
			}
		}
		return new Pair<>(answer, answerFlag);
	}

	//Алгоритм теста Миллера-Рабина
	public boolean isProbablePrimaryNumber() {
		if (this.getBinaryValidate().equals(ONE) || this.isZero())
			return false;
		else if ((this.getBinaryValidate().compareTo(new BinaryNumber("11")) <= 0))
			return true;
		else if ((isParity())) return false;
		else {
			BinaryNumber m = this, t = subOne(this);
			BinaryNumber mSubOne = subOne(m);
			BinaryNumber s = ZERO;
			while (t.isParity()) {
				t = divisionTwo(t);
				s = addOne(s);
			}

			int length = (int) length();
			BinaryNumber x, a;
			for (long i = 0; i < length; i++) {
				a = generate(2, length - 2, true);
				x = fastPowToMod(a, t, m);
				if (ONE.equalsBinaryValidate(x) || mSubOne.equalsBinaryValidate(x))
					continue;
				boolean b = false;
				for (BinaryNumber j = ZERO; j.compareTo(s) < 0; j = addOne(j)) {
					x = fastPowToMod(x, new BinaryNumber("10"), m);
					if (ONE.equals(x)) return false;
					if (mSubOne.equals(x)) {
						b = true;
						break;
					}
				}
				if (b) continue;

				return false;
			}
			return true;
		}
	}

	//Быстрое возведение  в степень
	public static BinaryNumber fastPow(BinaryNumber x, BinaryNumber n) {
		BinaryNumber answer = new BinaryNumber("1");
		if (x == null || n == null) throw new IllegalArgumentException();

		while (!n.isZero()) {

			if (!n.isParity()) {
				answer = fastMultiplication(answer, x).getBinaryValidate();
				n = subOne(n);
			} else {
				x = fastMultiplication(x, x).getBinaryValidate();
				n = divisionTwo(n);
			}
		}
		return answer;
	}

	//Быстрое возведение  в степень с модулем
	public static BinaryNumber fastPowToMod(BinaryNumber x, BinaryNumber n, BinaryNumber module) {
		BinaryNumber answer = mod(x, module), tmp;
		if (x == null || n == null) throw new IllegalArgumentException();
		if (module.equalsBinaryValidate(ONE))
			return ZERO;

		String s = n.toStringBinaryValidate();
		for (int i = 1, l = s.length(); i < l; i++) {
			//System.out.printf("%4s / %s\n", i, l);
			tmp = fastMultiplication(answer, answer);
			if ((s.charAt(i) == '0'))
				answer = mod(tmp, module);
			else
				answer = mod(fastMultiplication(tmp, x), module);
		}

		answer = mod(answer, module);

		return answer;
	}

	//Алгоритм Карацуби
	public static BinaryNumber fastMultiplication(BinaryNumber first, BinaryNumber second) {
		if (first.isZero() || second.isZero()) return ZERO;

		int length = Math.max(first.value.length, second.value.length);
		byte[] fBytes = first.value, sBytes = second.value;

		if (length < 10)
			return multiplication(first, second);

		if (length % 2 == 1 && length > 4) length++;
		fBytes = Util.addByteZero(fBytes, length);
		sBytes = Util.addByteZero(sBytes, length);

		int m = length / 2; //byte

		BinaryNumber a0 = new BinaryNumber(Arrays.copyOfRange(fBytes, fBytes.length - m, fBytes.length)),
				a1 = new BinaryNumber(Arrays.copyOfRange(fBytes, 0, fBytes.length - m)),
				b0 = new BinaryNumber(Arrays.copyOfRange(sBytes, sBytes.length - m, sBytes.length)),
				b1 = new BinaryNumber(Arrays.copyOfRange(sBytes, 0, sBytes.length - m));

		BinaryNumber s1 = fastMultiplication(a0, b0);

		BinaryNumber c1 = fastMultiplication(a1, b1);
		BinaryNumber c2 = fastMultiplication(a0, b0);
		BinaryNumber c3 =
				fastMultiplication(
						addition(a0, a1),
						addition(b0, b1)
				);
		BinaryNumber c4 = sub(c3, c2);
		BinaryNumber s2 = sub(c4, c1);

		s2 = new BinaryNumber(Util.concatArrayByte(s2.value, new byte[m]));
		BinaryNumber s3 = multiplication(a1, b1);
		s3 = new BinaryNumber(Util.concatArrayByte(s3.value, new byte[2 * m]));

		return addition(addition(s1, s2), s3);
	}

	//Четность числа
	public boolean isParity() {
		int l = value.length;
		return l != 0 && (value[l - 1] & 1) != 1;
	}

	//Обычное произведение
	public static BinaryNumber multiplication(BinaryNumber first, BinaryNumber second) {
		if (ZERO.equals(first) || ZERO.equals(second))
			return ZERO;

		String s = second.toString(), f = first.toString();
		BinaryNumber answer = new BinaryNumber("0");
		for (int size = s.length(), i = size - 1; i >= 0; i--) {
			if (s.charAt(i) == '1') {
				String add = "";
				if (size - 1 - i != 0)
					add = new String(new char[size - 1 - i]).replace('\0', '0');
				String tmp = f + add;
				answer = addition(answer, new BinaryNumber(tmp));
			}
		}
		return answer;
	}

	//Произведение карацубы с модулем
	public static BinaryNumber fastMultiplicationToMod(BinaryNumber first, BinaryNumber second, BinaryNumber module) {
		return mod(fastMultiplication(first, second), module);
	}

	//Сложение
	public static BinaryNumber addition(BinaryNumber first, BinaryNumber second) {
		int length = Math.max(first.value.length, second.value.length);
		byte[] fBytes = first.value, sBytes = second.value;

		fBytes = Util.addByteZero(fBytes, length);
		sBytes = Util.addByteZero(sBytes, length);


		byte[] answer = new byte[length];
		byte tmp = ZERO_BYTE;
		byte[] bytes;

		for (int i = length - 1; i >= 0; i--) {
			bytes = Util.additionTwoBinaryNumber(fBytes[i], sBytes[i], tmp);
			answer[i] = bytes[1];
			tmp = bytes[0];
		}
		if (tmp == 1)
			answer = Util.concatArrayByte(new byte[]{tmp}, answer);

		return new BinaryNumber(answer);
	}

	//Сложение с модулем
	public static BinaryNumber additionToMod(BinaryNumber first, BinaryNumber second, BinaryNumber module) {
		return mod(addition(first, second), module);
	}

	//Плюс 1
	public static BinaryNumber addOne(BinaryNumber number) {
		return addition(number, ONE);
	}

	//Минус 1
	public static BinaryNumber subOne(BinaryNumber number) {
		return sub(number, ONE);
	}

	//Деление на 2
	public static BinaryNumber divisionTwo(BinaryNumber number) {
		if (number.isZero()) return ZERO;
		String s = number.toString();
		return new BinaryNumber(s.substring(0, s.length() - 1));
	}

	//Вычитание
	public static BinaryNumber sub(BinaryNumber first, BinaryNumber second) {
		int c = first.compareTo(second);
		if (c < 0) throw new IllegalArgumentException();
		else if (c == 0) return ZERO;
		else {
			byte[] fBytes = first.value, sBytes = second.value;

			sBytes = Util.addByteZero(sBytes, fBytes.length);
			byte[] answer = new byte[fBytes.length];
			byte tmp = ZERO_BYTE;
			byte[] bytes;

			for (int size = first.value.length, i = size - 1; i >= 0; i--) {
				bytes = Util.subTwoBinaryNumber(fBytes[i], sBytes[i], tmp);
				answer[i] = bytes[1];
				tmp = bytes[0];
			}

			return new BinaryNumber(answer).getBinaryValidate();
		}
	}

	//Остаток от деления
	public static BinaryNumber mod(BinaryNumber number, BinaryNumber module) {
		int compareTo = number.compareTo(module);
		if (compareTo < 0)
			return number;
		else if (compareTo == 0 || module.equals(ONE))
			return ZERO;
		else {
			while (number.compareTo(module) >= 0) {
				String n = number.toStringBinaryValidate();
				String m = module.toStringBinaryValidate();
				int delta = n.length() - m.length();
				BinaryNumber newM = new BinaryNumber(m + new String(new char[delta]).replace('\0', '0'));
				if (newM.compareTo(number) > 0) newM = divisionTwo(newM);
				number = sub(number, newM);
			}

			return number;
		}
	}

	//Деление
	public static BinaryNumber division(BinaryNumber first, BinaryNumber second) {
		if (second.equalsBinaryValidate(ONE)) return first;
		int k = first.compareTo(second);
		if (k < 0) return ZERO;
		else if (k == 0) return ONE;
		else {
			/*StringBuilder answer = new StringBuilder("1");
			first = first.getBinaryValidate();
			second = second.getBinaryValidate();
			String sFirst = first.toStringBinaryValidate(), sSecond = second.toStringBinaryValidate();

			int length_byte = Math.max(first.value.length,second.value.length);
			byte[] firstValue = Util.addByteZero(first.value,length_byte);
			byte[] secondValue = Util.addByteZero(second.value,length_byte);

			byte[] tmp = secondValue;
			int l = sFirst.length() - sSecond.length();
			for (int i = 0; i < length_byte; i++) {
				tmp[i] = (byte) (tmp[i] << l);
			}

			byte[] tmp2 = new byte[length_byte];
			for (int i = 0; i < length_byte; i++) {
				tmp2[i] = (byte) (~tmp2[i]);
			}
			tmp2 = addOne(new BinaryNumber(tmp2)).getValue();

			while (true) {

			}*/
			StringBuilder answer = new StringBuilder("1");
			String sFirst = first.toStringBinaryValidate(), sSecond = second.toStringBinaryValidate();
			int flag = (int) sSecond.length();
			BinaryNumber tmp = new BinaryNumber(sFirst.substring(0, flag));
			if (tmp.compareTo(second) < 0) {
				flag++;
				tmp = new BinaryNumber(sFirst.substring(0, flag));
			}
			tmp = sub(tmp, second);
			while (true) {
				if (flag == sFirst.length()) break;
				{
					tmp = multiplication(tmp, TWO);
					if (sFirst.charAt(flag) == '1')
						tmp = addOne(tmp);
					flag++;

					while (tmp.compareTo(second) < 0) {
						if (flag == sFirst.length()) break;
						tmp = multiplication(tmp, TWO);
						if (sFirst.charAt(flag) == '1')
							tmp = addOne(tmp);
						flag++;
						answer.append("0");
					}

					if (tmp.compareTo(second) < 0) {
						answer.append("0");
					} else {
						answer.append("1");
						tmp = sub(tmp, second);
					}
				}
			}
			return new BinaryNumber(answer.toString());
		}
	}


	public long length() {
		return value.length * 8;
	}

	//Длина бинарного числа
	public long lengthBinaryValidate() {
		if (value.length == 0)
			return 0;
		String s = toStringBinaryValidate();
		return s.length();
	}

	//Проверка на валидность числа
	public boolean validate(String s) {
		for (int i = 0, size = s.length(); i < size; i++) {
			char c = s.charAt(i);
			if (c != '1' && c != '0')
				return false;
		}
		return true;
	}

	//Проверка на 0
	public boolean isZero() {
		for (byte aValue : value)
			if (aValue != ZERO_BYTE)
				return false;

		return true;
	}

	public byte[] getValue() {
		return value;
	}

	//Подсчет старших нулей
	public BinaryNumber considerFirstZero() {
		for (byte aValue : value) {
			if (aValue == 0)
				firstZero += 8;
			else {
				String s = Util.byteToString(aValue);
				firstZero += s.indexOf("1");
				break;
			}
		}
		return this;
	}


	//Xor
	public static BinaryNumber xor(BinaryNumber first, BinaryNumber second, int lengthByte) {
		byte[]
				fBytes = Util.addByteZero(first.value, lengthByte),
				sBytes = Util.addByteZero(second.value, lengthByte);

		byte[] bytes = new byte[lengthByte];
		for (int i = 0; i < lengthByte; i++)
			bytes[i] = (byte) (fBytes[i] ^ sBytes[i]);
		return new BinaryNumber(bytes);
	}

	//Хеш SHA512
	public static BinaryNumber getSHA512(BinaryNumber number) {
		return new BinaryNumber(Sha512.getSHA512(number.getValue()));
	}


	//Валидное двоичное число в стринговой форме
	public String toStringBinaryValidate() {
		if (value.length == 0) return "";
		StringBuilder answer = new StringBuilder();

		for (byte aValue : value)
			answer.append(Util.byteToString(aValue));

		return (answer.indexOf("1") == -1) ?
				"0" :
				new String(new char[firstZero]).replace('\0', '0') + answer.delete(0, answer.indexOf("1")).toString();
	}

	@Override
	public String toString() {
		if (value.length == 0) return "";
		StringBuilder answer = new StringBuilder();

		for (byte aValue : value)
			answer.append(Util.byteToString(aValue));
		return new String(new char[firstZero]).replace('\0', '0') + answer.toString();
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof BinaryNumber && Arrays.equals(value, ((BinaryNumber) obj).value);
	}

	public boolean equalsBinaryValidate(Object obj) {
		return obj instanceof BinaryNumber && toStringBinaryValidate().equals(((BinaryNumber) obj).toStringBinaryValidate());
	}

	//Сравнение
	@Override
	public int compareTo(BinaryNumber o) {
		String s1 = toStringBinaryValidate(), s2 = o.toStringBinaryValidate();
		if (s1.length() != s2.length()) {
			return Integer.compare(s1.length(), s2.length());
		} else {
			for (int i = 0; i < s1.length(); i++) {
				if (s1.charAt(i) != s2.charAt(i)) {
					return Character.compare(s1.charAt(i), s2.charAt(i));
				}
			}
		}
		return 0;
	}

	static class Pair<T, K> {
		public T first;
		public K second;

		public Pair(T first, K second) {
			this.first = first;
			this.second = second;
		}
	}
}
