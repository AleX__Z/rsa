package com.company.lang;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Random;

public class RSA {
	public static final int XOR_BYTE = 64;
	public static final int RANDOM_BIT = 512;

	//Ключи
	public Key publicKey, privateKey;



	private static int[] firstPrimaryNumber = new int[]
			{
					2,
					3,
					5,
					7,
					11,
					13,
					17,
					19,
					23,
					29,
					31,
					37,
					41,
					43,
					47,
					53,
					59,
					61,
					67,
					71,
					73,
					79,
					83,
					89,
					97,
					101,
					103,
					107,
					109,
					113,
					127,
					131,
					137,
					139,
					149,
					151,
					157,
					163,
					167,
					173,
					179,
					181,
					191,
					193,
					197,
					199,
					211,
					223,
					227,
					229,
					233,
					239,
					241,
					251
			};
	private static BinaryNumber[] binaryNumbers;

	static {
		binaryNumbers = new BinaryNumber[firstPrimaryNumber.length];
		for (int i = 0, l = firstPrimaryNumber.length; i < l; i++) {
			binaryNumbers[i] = new BinaryNumber(Integer.toBinaryString(firstPrimaryNumber[i]));
		}
	}

	public RSA(Key publicKey, Key privateKey) {
		this.publicKey = publicKey;
		this.privateKey = privateKey;
	}

	public RSA() {

	}


	//Генерация ключей
	public static Key[] generateKeys() {
		BinaryNumber p, q;
		BinaryNumber TWO = BinaryNumber.addition(BinaryNumber.ONE, BinaryNumber.ONE);
		boolean flag = false;
		p = BinaryNumber.generateNoParity(513, true);

		do {
			flag = false;

			for (int i = 0, l = firstPrimaryNumber.length; i < l; i++) {
				System.out.println(i);
				if (BinaryNumber.mod(p, binaryNumbers[i]).isZero()) {
					flag = true;
					break;
				}
			}

			if (!flag) {
				flag = !p.isProbablePrimaryNumber();
			}

			if (flag) {
				p = BinaryNumber.addition(p, TWO);
			}

		} while (flag);

		q = BinaryNumber.generateNoParity(512, true);
		do {
			flag = false;
			for (int i = 0, l = firstPrimaryNumber.length; i < l; i++) {
				if (BinaryNumber.mod(q, binaryNumbers[i]).isZero()) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				flag = !q.isProbablePrimaryNumber();
			}
			if (flag) {
				q = BinaryNumber.addition(q, TWO);
			}
		} while (flag);


		return get2Keys(p, q);
	}

	//Быстрая генерация ключей
	public static Key[] generate2Keys(int l) {
		BigInteger[] bigIntegers = new BigInteger[]{
				BigInteger.probablePrime((l / 2) + 1, new Random()),
				BigInteger.probablePrime((l / 2) + 1, new Random())
		};

		BinaryNumber
				p = new BinaryNumber(bigIntegers[0].toString(2)),
				q = new BinaryNumber(bigIntegers[1].toString(2));

		return get2Keys(p, q);
	}

	public static Key[] get2Keys(BinaryNumber q, BinaryNumber p) {

		BinaryNumber n = BinaryNumber.fastMultiplication(p, q);
		BinaryNumber phiN = BinaryNumber.fastMultiplication(BinaryNumber.subOne(p), BinaryNumber.subOne(q));
		BinaryNumber e = BinaryNumber.ONE, d;
		BinaryNumber ONE = new BinaryNumber("1");
		for (BinaryNumber i = BinaryNumber.TWO; i.compareTo(phiN) < 0; i = BinaryNumber.addition(i, ONE))
			if (BinaryNumber.findGcd(i, phiN).equals(ONE) &&
					i.toStringBinaryValidate().replace('0', '\0').length() > i.toStringBinaryValidate().length() / 2) {
				e = i;
				break;
			}

		d = BinaryNumber.findExtendedEuclidean(phiN, e);
		return new Key[]{new Key(e, n), new Key(d, n)};
	}


	public static BinaryNumber encryptOAEP(BinaryNumber number) {
		return encryptOAEP(number, BinaryNumber.generate(RANDOM_BIT, false));
	}

	//OAEP шифрование
	public static BinaryNumber encryptOAEP(BinaryNumber number, BinaryNumber random) {
		if (number.lengthBinaryValidate() > 510 || random.lengthBinaryValidate() > 512) {
			throw new IllegalArgumentException();
		}
		String s = number.toStringBinaryValidate();
		String s1 = s + "01" + new String(new char[RANDOM_BIT - s.length() - 2]).replace("\0", "0");
		number = new BinaryNumber(s1);
		random = new BinaryNumber(Util.addByteZero(random.getValue(), RANDOM_BIT / 8));

		BinaryNumber shaRandom = BinaryNumber.getSHA512(random);
		BinaryNumber x1 = BinaryNumber.xor(number, shaRandom, XOR_BYTE);
		BinaryNumber shaX = BinaryNumber.getSHA512(x1);
		BinaryNumber y1 = BinaryNumber.xor(shaX, random, XOR_BYTE);
		return new BinaryNumber(Util.concatArrayByte(x1.getValue(), y1.getValue()));
	}

	//OAEP расшифровка
	public static BinaryNumber decryptOAEP(BinaryNumber number) {
		byte[] value = number.getValue();
		int l = value.length / 2;
		BinaryNumber x = new BinaryNumber(Arrays.copyOfRange(value, 0, l));
		BinaryNumber y = new BinaryNumber(Arrays.copyOfRange(value, l, value.length));
		BinaryNumber shaX_1 = BinaryNumber.getSHA512(x);
		BinaryNumber random = BinaryNumber.xor(shaX_1, y, XOR_BYTE);
		BinaryNumber message = BinaryNumber.xor(x, BinaryNumber.getSHA512(random), XOR_BYTE).considerFirstZero();
		String mes = message.toStringBinaryValidate();
		mes = mes.substring(0, mes.lastIndexOf("1") - 1);
		return new BinaryNumber(mes, true);
	}

	public static BinaryNumber[] decrypt2OAEP(BinaryNumber number) {
		byte[] value = number.getValue();
		int l = value.length / 2;
		BinaryNumber x = new BinaryNumber(Arrays.copyOfRange(value, 0, l));
		BinaryNumber y = new BinaryNumber(Arrays.copyOfRange(value, l, value.length));
		BinaryNumber shaX_1 = BinaryNumber.getSHA512(x);
		BinaryNumber random = BinaryNumber.xor(shaX_1, y, XOR_BYTE);
		BinaryNumber message = BinaryNumber.xor(x, BinaryNumber.getSHA512(random), XOR_BYTE).considerFirstZero();
		String mes = message.toStringBinaryValidate();
		mes = mes.substring(0, mes.lastIndexOf("1") - 1);
		return new BinaryNumber[]{new BinaryNumber(mes, true), random.getBinaryValidate()};
	}

	//RSA шифровка
	public BinaryNumber encryptRSA(BinaryNumber number) {
		if (publicKey == null) {
			Key[] keys = generate2Keys((int) (number.lengthBinaryValidate() + 1));
			publicKey = keys[0];
			privateKey = keys[1];
		}
		if (publicKey.second.compareTo(number) <= 0)
			throw new IllegalArgumentException("Length N > p*q");

		return BinaryNumber.fastPowToMod(number, publicKey.first, publicKey.second);

	}

	//RSA расшифровка
	public BinaryNumber decryptRSA(BinaryNumber number) {
		if (privateKey == null) throw new NullPointerException("Private key is null");
		BinaryNumber answer = BinaryNumber.fastPowToMod(
				number,
				privateKey.first, privateKey.second
		).getBinaryValidate();

		return answer;
	}
}
