package com.company.lang;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class Util {
	public static byte[] concatArrayByte(byte[] A, byte[] B) {
		int aLen = A.length;
		int bLen = B.length;
		byte[] C = new byte[aLen + bLen];
		System.arraycopy(A, 0, C, 0, aLen);
		System.arraycopy(B, 0, C, aLen, bLen);
		return C;
	}

	public static String byteToString(byte b) {
		return String.format("%8s", Integer.toBinaryString(b & 0xFF)).replace(' ', '0');
	}

	public static byte stringToByte(String s) {
		if (s.length() != 8) throw new IllegalArgumentException();
		return (byte) Integer.parseInt(s, 2);
	}

	public static byte[] longToBytes(long l) {
		ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
		buffer.putLong(l);
		return buffer.array();
	}

	public static byte[] subTwoBinaryNumber(byte first, byte second, byte third) {
		int f = (first < 0) ? first + 256 : first;
		int s = (second < 0) ? second + 256 : second;
		//int t = (third < 0) ? third + 256 : third;
		int sub = f - s - third;
		byte b1 = (sub < 0) ? (byte) 1 : (byte) 0;
		sub = (sub < 0) ? sub + 256 : sub;
		return new byte[]{b1, (byte) (sub)};
	}

	public static byte[] additionTwoBinaryNumber(byte first, byte second, byte third) {
		int f = (first < 0) ? first + 256 : first;
		int s = (second < 0) ? second + 256 : second;
		//int t = (third < 0) ? third + 256 : third;
		int sum = f + s + third;
		byte b1 = (sum > 255) ? (byte) 1 : (byte) 0;
		return new byte[]{b1, (byte) (first + second + third)};
	}

	public static byte[] addByteZero(byte[] bytes, int length) {
		int oldLength = bytes.length;
		if (oldLength < length)
			bytes = Util.concatArrayByte(new byte[length - oldLength], bytes);
		return bytes;
	}
}
