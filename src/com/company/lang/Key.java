package com.company.lang;

public class Key {
	public BinaryNumber first, second;

	public Key(BinaryNumber first, BinaryNumber second) {
		this.first = first;
		this.second = second;
	}

	public Key(String key) {
		key = key.replaceAll(" ", "");
		String[] s = key.substring(1, key.length() - 1).split(",");
		first = new BinaryNumber(s[0]);
		second = new BinaryNumber(s[1]);
	}

	public Key(Key key) {
		this.first = key.first;
		this.second = key.second;
	}

	@Override
	public String toString() {
		return String.format("{%s , %s}", first.toString(), second.toString());
	}

}
