package com.company;

import com.company.lang.BinaryNumber;
import com.company.lang.RSA;

import java.io.*;

public class OAEPMain {
	public static void main(String[] args) throws IOException {
		try (
				BufferedReader in = new BufferedReader(new FileReader("in_oaep.txt"));
				PrintWriter out_decrypt = new PrintWriter(new BufferedWriter(new FileWriter("out_oaep_decrypt.txt")));
				PrintWriter out_encrypt = new PrintWriter(new BufferedWriter(new FileWriter("out_oaep_encrypt.txt")))
		) {
			BinaryNumber number = RSA.encryptOAEP(new BinaryNumber(in.readLine(), true));
			out_encrypt.println(number.toStringBinaryValidate());
			out_decrypt.println(RSA.decryptOAEP(number));
		}
	}
}
