package com.company;

import com.company.lang.BinaryNumber;
import com.company.lang.Key;
import com.company.lang.RSA;
import com.company.lang.Sha512;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	public static void main(String[] args) throws IOException {
		try (BufferedReader in = new BufferedReader(new InputStreamReader(System.in))) {
			System.out.println("Описание команд можно посмотреть при помощи команды \"help\" :");
			String[] split;
			//System.out.println(BinaryNumber.generate(512,true));

			while (true) {
				try {
					split = in.readLine().split(" ");
					switch (split[0]) {
						case "quit":
							System.exit(0);
							break;
						case "help":
							System.out.println("Команды : \n" +
									"- sha512 <бинарное число> - получить хеш SHA512 от бинаного числа\n" +
									"- primeNumber <кол-во бит> - сгенерировать простое число\n" +
									"- rsa <простые бинарное число> <простое бинорное число> <бинарное число> - зашифровать бинарное число и вывести ключи вместе с защифрованным сообщением\n" +
									"- oaep <бинарное число> <рандомное бинарное число> - Алгоритм OAEP\n" +
									"- decrypt_oaep <бинарное число> - расшифровка oaep\n" +
									"- mulMod <бинарное число> <бинарное число> <бинарное число> - a*b mod n (произведение Карацуби) \n" +
									"- addMod <бинарное число> <бинарное число> <бинарное число> - a+b mod n \n" +
									"- powMod <бинарное число> <бинарное число> <бинарное число> - a^b mod n \n" +
									"- inverseMod <бинарное число> <бинарное число> - a^(-1) mod n  (Алгоритм Евклида)\n" +
									"- quit - выход");

							break;
						case "sha512":
							if (split.length != 2) throw new ProgramException();
							System.out.println(new BinaryNumber(Sha512.getSHA512(split[1])));
							//System.out.println(BinaryNumber.getSHA512(new BinaryNumber(split[1], true)));
							break;
						case "primeNumber":
							if (split.length != 2) throw new ProgramException();
							BinaryNumber number;

							do {
								number = BinaryNumber.generate(Integer.parseInt(split[1]), true);
							} while (!number.isProbablePrimaryNumber());

							System.out.println(number.toStringBinaryValidate());
							break;
						case "rsa":
							if (split.length != 4) throw new ProgramException();
							Key[] keys = RSA.get2Keys(new BinaryNumber(split[1]), new BinaryNumber(split[2]));
							RSA rsa = new RSA(keys[0], keys[1]);
							System.out.println("Публичный ключ " + keys[0].toString());
							System.out.println("Приватный ключ " + keys[1].toString());
							System.out.println(rsa.encryptRSA(new BinaryNumber(split[3], true)));
							break;
						case "oaep":
							if (split.length != 3) throw new ProgramException();
							System.out.println(RSA.encryptOAEP(new BinaryNumber(split[1]), new BinaryNumber(split[2])));
							break;
						case "decrypt_oaep":
							if (split.length != 2) throw new ProgramException();
							BinaryNumber number1 = new BinaryNumber(split[1]);
							if (number1.length() != 1024) throw new ProgramException();
							BinaryNumber[] numbers = RSA.decrypt2OAEP(number1);
							System.out.println("Рандом " + numbers[1].toStringBinaryValidate());
							System.out.println(numbers[0].toStringBinaryValidate());
							break;
						case "mulMod":
							if (split.length != 4) throw new ProgramException();
							System.out.println(
									BinaryNumber.fastMultiplicationToMod(
											new BinaryNumber(split[1]),
											new BinaryNumber(split[2]),
											new BinaryNumber(split[3])
									)
							);
							break;
						case "addMod":
							if (split.length != 4) throw new ProgramException();
							System.out.println(
									BinaryNumber.additionToMod(
											new BinaryNumber(split[1]),
											new BinaryNumber(split[2]),
											new BinaryNumber(split[3])
									)
							);
							break;
						case "powMod":
							if (split.length != 4) throw new ProgramException();
							System.out.println(
									BinaryNumber.fastPowToMod(
											new BinaryNumber(split[1]),
											new BinaryNumber(split[2]),
											new BinaryNumber(split[3])
									)
							);
							break;
						case "inverseMod":
							if (split.length != 3) throw new ProgramException();
							System.out.println(BinaryNumber.findExtendedEuclidean(new BinaryNumber(split[2]), new BinaryNumber(split[1])));
							break;

						default:
							System.out.println("Несуществующая команда");
					}
				} catch (ProgramException e) {
					System.out.println("Ошибка");
				}

			}

		}
	}

	private static class ProgramException extends Exception {
	}
}
