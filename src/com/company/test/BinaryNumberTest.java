package com.company.test;

import com.company.lang.BinaryNumber;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;
import java.security.MessageDigest;

public class BinaryNumberTest extends Assert {
	@Test
	public void testFindGcd() throws Exception {


	}

	@Test
	public void testIsProbablePrimaryNumber() throws Exception {

	}

	@Test
	public void testFastPow() throws Exception {
		for (int x = 0; x < Math.pow(10, 1); x++) {
			for (int n = 0; n < 6; n++) {
				assertEquals(
						Long.toBinaryString((long) Math.pow(x, n)),
						BinaryNumber.fastPow(
								new BinaryNumber(Integer.toBinaryString(x)),
								new BinaryNumber(Integer.toBinaryString(n))
						).toStringBinaryValidate()
				);
			}
		}
	}

	@Test
	public void testFastPowToMod() throws Exception {
		for (int x = 1; x < Math.pow(2, 5); x++) {
			for (int n = 1; n < 6; n++) {
				for (int module = 1; module < Math.pow(2, 30); module++) {

					long tmp = (long) (Math.pow(x, n) % module);

					BinaryNumber number = BinaryNumber.fastPowToMod(
							new BinaryNumber(Integer.toBinaryString(x)),
							new BinaryNumber(Integer.toBinaryString(n))
							, new BinaryNumber(Integer.toBinaryString(module)));

					assertEquals(
							Long.toBinaryString(tmp),
							number.toStringBinaryValidate()
					);
				}

			}
		}
	}

	@Test
	public void testFastMultiplication() throws Exception {
		for (int a = 0; a < Math.pow(10, 2); a++) {
			for (int b = 0; b < Math.pow(10, 3); b++) {
				assertEquals(
						Integer.toBinaryString(a * b),
						BinaryNumber.fastMultiplication(
								new BinaryNumber(Integer.toBinaryString(a)),
								new BinaryNumber(Integer.toBinaryString(b))
						).toStringBinaryValidate()
				);
			}
		}
	}

	@Test
	public void test1() {
		BinaryNumber x = new BinaryNumber("10000000000000110100011");
		BinaryNumber module = new BinaryNumber("100010111111100111111111");
//		10010110110101001011101
//		11000000001100001111010
		BinaryNumber p1 = BinaryNumber.multiplication(x, x);
		BinaryNumber p2 = BinaryNumber.fastMultiplication(x, x);
		System.out.println(Long.parseLong("10000000000000110100011", 2));
		System.out.println(Long.parseLong(p1.toStringBinaryValidate(), 2));
		System.out.println(Long.parseLong(p2.toStringBinaryValidate(), 2));
/*


		BinaryNumber number = BinaryNumber.mod(p1.makeBinaryValidate(), module);
		BinaryNumber number1 = BinaryNumber.mod(p2.makeBinaryValidate(), module);
		System.out.println(number.toStringBinaryValidate());
		System.out.println(number1.toStringBinaryValidate());
*/


	}

	@Test
	public void testIsParity() throws Exception {
		for (int i = 0; i < Math.pow(10, 4); i++) {
			assertEquals(i % 2 == 0, new BinaryNumber(Integer.toBinaryString(i)).isParity());
		}
	}

	@Test
	public void testMultiplication() throws Exception {
		for (int a = 0; a < Math.pow(10, 2); a++) {
			for (int b = 0; b < Math.pow(10, 3); b++) {
				assertEquals(
						Integer.toBinaryString(a * b),
						BinaryNumber.multiplication(
								new BinaryNumber(Integer.toBinaryString(a)),
								new BinaryNumber(Integer.toBinaryString(b))
						).toStringBinaryValidate()
				);
			}
		}
	}

	@Test
	public void testMultiplicationToModule() throws Exception {

	}

	@Test
	public void testAddition() throws Exception {
		for (int a = 0; a < Math.pow(10, 2); a++) {
			for (int b = 0; b < Math.pow(10, 3); b++) {
				assertEquals(
						Integer.toBinaryString(a + b),
						BinaryNumber.addition(
								new BinaryNumber(Integer.toBinaryString(a)),
								new BinaryNumber(Integer.toBinaryString(b))
						).toStringBinaryValidate()
				);
			}
		}
	}

	@Test
	public void testAdditionToModule() throws Exception {

	}

	@Test
	public void testSubOne() throws Exception {
		for (int a = 1; a < Math.pow(10, 3); a++) {
			assertEquals(
					Integer.toBinaryString(a - 1),
					BinaryNumber.subOne(
							new BinaryNumber(Integer.toBinaryString(a))
					).toStringBinaryValidate()
			);
		}
	}

	@Test
	public void testDivisionTwo() throws Exception {
		for (int i = 0; i < Math.pow(10, 4); i++) {
			assertEquals(
					Integer.toBinaryString(i / 2),
					BinaryNumber.divisionTwo(new BinaryNumber(Integer.toBinaryString(i))).toStringBinaryValidate()
			);
		}
	}

	@Test
	public void testSub() throws Exception {
		for (int a = 0; a < Math.pow(10, 3); a++) {
			for (int b = 0; b <= a; b++) {
				assertEquals(
						Integer.toBinaryString(a - b),
						BinaryNumber.sub(
								new BinaryNumber(Integer.toBinaryString(a)),
								new BinaryNumber(Integer.toBinaryString(b))
						).toStringBinaryValidate()
				);
			}
		}
	}

	@Test
	public void testMod() throws Exception {
		for (int a = 1; a < Math.pow(2, 20); a++) {
			for (int b = 1; b < Math.pow(2, 20); b++) {
				assertEquals(
						Integer.toBinaryString(a % b),
						BinaryNumber.mod(
								new BinaryNumber(Integer.toBinaryString(a)),
								new BinaryNumber(Integer.toBinaryString(b))
						).toStringBinaryValidate()
				);
			}
		}
	}

	@Test
	public void testLengthBinaryValidate() throws Exception {
		for (int i = 0; i < Math.pow(10, 4); i++) {
			assertEquals(new BinaryNumber(Integer.toBinaryString(i)).lengthBinaryValidate(), Integer.toBinaryString(i).length());
		}
	}

	/*@Test
	public void testValidate() throws Exception {

	}*/

	/*@Test
	public void testIsZero() throws Exception {

	}*/

	/*@Test
	public void testGetValue() throws Exception {

	}*/

	@Test
	public void testSHA512() throws Exception {
		String s = "abc";
		BinaryNumber number = BinaryNumber.getSHA512(new BinaryNumber(s.getBytes()));
		MessageDigest mda = MessageDigest.getInstance("SHA-512");
		byte[] e = mda.digest(s.getBytes());
		System.out.println(number.toString());
		assertArrayEquals(e, number.getValue());

	}

	@Test
	public void testXor512() throws Exception {
		for (int a = 0; a < Math.pow(10, 2); a++) {
			for (int b = 0; b < Math.pow(10, 2); b++) {
				assertEquals(
						Integer.toBinaryString(a ^ b),
						BinaryNumber.xor(
								new BinaryNumber(Integer.toBinaryString(a)),
								new BinaryNumber(Integer.toBinaryString(b))
								, 64).toStringBinaryValidate()
				);
			}
		}
	}


	@Test
	public void testDivision() throws Exception {
		for (int a = 1; a < Math.pow(10, 3); a++) {
			for (int b = 1; b < Math.pow(10, 2); b++) {
				System.out.println(a + " " + b);
				assertEquals(
						Integer.toBinaryString(a / b),
						BinaryNumber.division(
								new BinaryNumber(Integer.toBinaryString(a)),
								new BinaryNumber(Integer.toBinaryString(b))
						).toStringBinaryValidate()
				);
			}
		}
	}

	@Test
	public void testToStringBinaryValidate() throws Exception {
		for (int i = 0; i < 512; i++) {
			char[] chars = new char[i];
			if (i != 0) chars[0] = '1';
			for (int j = 1, size = chars.length; j < size; j++) {
				chars[j] = (char) ('0' + (int) (Math.random() * 2));
			}
			String s = new String(chars);
			assertEquals(s, new BinaryNumber(s).toStringBinaryValidate());
		}
	}

	@Test
	public void testEquals() throws Exception {
		for (int i = 0; i < 512; i++) {
			BinaryNumber n = BinaryNumber.generate(i, true);
			BinaryNumber m = new BinaryNumber(n.getValue());
			BinaryNumber o = new BinaryNumber(n.toStringBinaryValidate());
			assertTrue(n.equals(m));
			assertTrue(n.equals(o));
			assertTrue(m.equals(o));
			assertTrue(o.equals(n));
		}
	}

	@Test
	public void testCompareTo() throws Exception {
		for (int a = 0; a < Math.pow(10, 2); a++) {
			for (int b = 0; b < Math.pow(10, 3); b++) {
				assertEquals(
						new Integer(a).compareTo(b),
						new BinaryNumber(Integer.toBinaryString(a)).compareTo(
								new BinaryNumber(Integer.toBinaryString(b))
						)
				);
			}
		}
	}
}