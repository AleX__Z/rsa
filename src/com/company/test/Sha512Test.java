package com.company.test;

import com.company.lang.Sha512;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.security.MessageDigest;

import static org.junit.Assert.*;

public class Sha512Test extends Assert {

	/*@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {

	}*/

	@Test
	public void testSha512() throws Exception {
		String s = "kljasbchjasdgfasdkvlnf";
		byte[] bytes = s.getBytes();
		MessageDigest mda = MessageDigest.getInstance("SHA-512");
		byte[] e = mda.digest(bytes);
		byte[] a = Sha512.getSHA512(bytes);

		assertArrayEquals(e, a);
	}
}